package br.ucb.financasonline.bo;

import java.util.Date;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.ucb.financasonline.entidade.Caixa;
import br.ucb.financasonline.negocio.CaixaBO;

public class CaixaBOTest {

	private CaixaBO caixaBo;
	private Caixa caixa;

	@Before
	public void inicialize() {
		caixa = new Caixa();
		caixa.setDescricao("Caixa 1");
		caixa.setData(new Date(2013, 11, 03));
		caixa.setSaldo(200);
		
		caixaBo = new CaixaBO();
	}

	@After
	public void tearDown() {
		caixaBo.excluir(caixa);
	}

	@Test
	public void inserirCaixa() {
		
		caixaBo.inserir(caixa);
		Assert.assertTrue(caixa.getId() != null);
	}

	@Test
	public void obterCaixa() {

		caixaBo.inserir(caixa);
		
		Caixa caixaTest = caixaBo.obterPorId(caixa.getId());
		Assert.assertTrue(caixaTest != null);
		Assert.assertTrue(caixaTest.getId() == caixa.getId());
	}

	@Test
	public void alterarCaixa() {
		caixaBo.inserir(caixa);
		
		caixa.setDescricao("Caixa alterado");
		caixaBo.atualizar(caixa);
		Assert.assertEquals("Caixa alterado", caixa.getDescricao());
	}

	@Test
	public void excluirCaixa() {

		caixaBo.inserir(caixa);	
		Long id = caixa.getId();
		caixaBo.excluir(caixa);
		Caixa caixaTest = caixaBo.obterPorId(id);
		Assert.assertTrue(caixaTest == null);
	}
}
