package br.ucb.financasonline.bo;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

//import br.ucb.financasonline.entidade.Filho;
//import br.ucb.financasonline.entidade.Pai;
//import br.ucb.financasonline.negocio.PaiBO;

public class PaiBoTest {
/*
	@Test
	public void atestInserir() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorId(pai.getId());
		
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();

		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
	}
	
	@Test
	public void etestObterPorId() {
		PaiBO paiBO = new PaiBO();
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		
		Pai paiNoBanco = paiBO.obterPorId(new Long(1));
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();

		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
	}
	
	@Test
	public void itestObterPorNome() {
		PaiBO paiBO = new PaiBO();
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		
		Pai paiNoBanco = paiBO.obterPorNome("JOAO SOBRENOME");
		
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
	}
	@Test
	public void otestAlterar() {
		PaiBO paiBO = new PaiBO();
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		
		Pai paiNoBanco = paiBO.obterPorId(new Long(1));
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			filho.setNome(filho.getNome() + " ALTERADO");
		}
		
		paiNoBanco.setNome("NOME ALTERADO");
		paiBO.atualizar(paiNoBanco);
		
		Pai paiAlteradoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		
		Assert.assertTrue(paiAlteradoNoBanco!=null);
		Assert.assertTrue(paiAlteradoNoBanco.getId()!=null);
		Assert.assertTrue(paiAlteradoNoBanco.getNome().equals("NOME ALTERADO"));
		
		Set<Filho> filhosAlteradosNoBanco = paiAlteradoNoBanco.getFilhos();

		for (Filho filho : filhosAlteradosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()+ " ALTERADO") || filho.getNome().equals(filhoGiovane.getNome()+ " ALTERADO"));
		}
	}
	
	
	@Test
	public void utestExcluir() {
		PaiBO paiBO = new PaiBO();
		
		Pai pai = paiBO.obterPorId(new Long(1));
		paiBO.excluir(pai);
		Pai paiNoBanco = paiBO.obterPorId(new Long(1));
		Assert.assertTrue(paiNoBanco==null);
	}
	

	@Test
	public void testInserir() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorId(pai.getId());
		
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
		paiBO.excluir(paiNoBanco);
		Pai paiExcluidoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		Assert.assertTrue(paiExcluidoNoBanco==null);
	}
	@Test
	public void testAlterar() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorId(pai.getId());
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			filho.setNome(filho.getNome() + " ALTERADO");
		}
		
		paiNoBanco.setNome("NOME ALTERADO");
		paiBO.atualizar(paiNoBanco);
		
		Pai paiAlteradoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		
		Assert.assertTrue(paiAlteradoNoBanco!=null);
		Assert.assertTrue(paiAlteradoNoBanco.getId()!=null);
		Assert.assertTrue(paiAlteradoNoBanco.getNome().equals("NOME ALTERADO"));
		
		Set<Filho> filhosAlteradosNoBanco = paiAlteradoNoBanco.getFilhos();
		
		for (Filho filho : filhosAlteradosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()+ " ALTERADO") || filho.getNome().equals(filhoGiovane.getNome()+ " ALTERADO"));
		}
		paiBO.excluir(paiAlteradoNoBanco);
		Pai paiExcluidoNoBanco = paiBO.obterPorId(paiAlteradoNoBanco.getId());
		Assert.assertTrue(paiExcluidoNoBanco==null);
	}
	@Test
	public void testObterPorId() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorId(pai.getId());
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
		paiBO.excluir(paiNoBanco);
		Pai paiExcluidoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		Assert.assertTrue(paiExcluidoNoBanco==null);
	}
	@Test
	public void testObterPorNome() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorNome(pai.getNome());
		
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
		paiBO.excluir(paiNoBanco);
		Pai paiExcluidoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		Assert.assertTrue(paiExcluidoNoBanco==null);
	}
	@Test
	public void testExcluir() {
		PaiBO paiBO = new PaiBO();
		Pai pai = new Pai();
		pai.setNome("JOAO SOBRENOME");
		
		
		Filho filhoCarlos = new Filho();
		filhoCarlos.setNome("CARLOS SOBRENOME");
		filhoCarlos.setPai(pai);
		
		Filho filhoGiovane = new Filho();
		filhoGiovane.setNome("GIOVANE SOBRENOME");
		filhoGiovane.setPai(pai);
		
		Set<Filho> filhos = new HashSet<Filho>();
		
		filhos.add(filhoCarlos);
		filhos.add(filhoGiovane);
		
		pai.setFilhos(filhos);
		
		paiBO.inserir(pai);
		
		Pai paiNoBanco = paiBO.obterPorNome(pai.getNome());
		
		Assert.assertTrue(paiNoBanco!=null);
		Assert.assertTrue(paiNoBanco.getId()!=null);
		Assert.assertTrue(paiNoBanco.getNome().equals("JOAO SOBRENOME"));
		
		Set<Filho> filhosNoBanco = paiNoBanco.getFilhos();
		
		for (Filho filho : filhosNoBanco) {
			Assert.assertTrue(filho.getNome().equals(filhoCarlos.getNome()) || filho.getNome().equals(filhoGiovane.getNome()));
		}
		
		paiBO.excluir(paiNoBanco);
		Pai paiExcluidoNoBanco = paiBO.obterPorId(paiNoBanco.getId());
		Assert.assertTrue(paiExcluidoNoBanco==null);
	}
		*/
}
