package br.ucb.financasonline.bo;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;
import br.ucb.financasonline.entidade.Usuario;
import br.ucb.financasonline.negocio.UsuarioBO;

public class UsuarioBOTest {
	
	@After
	public void tearDown() {

	}

	@Test
	public void inserirUsuario() {
		UsuarioBO uBO = new UsuarioBO();
		Usuario user = new Usuario();
		user.setEmail("thiagolyma@gmail.com");
		user.setNome("Thiago");
		user.setSenha("abc123");
		
		uBO.inserir(user);
		Usuario userBanco = uBO.obterPorId(user.getId());
		Assert.assertTrue(userBanco != null);
		Assert.assertTrue(userBanco.getId() != null);
		uBO.excluir(userBanco);
	}

	@Test
	public void excluirUsuario() {
		Usuario user = new Usuario();
		UsuarioBO uBO = new UsuarioBO();
		
		user.setEmail("thiagolyma@gmail.com");
		user.setNome("Thiago");
		user.setSenha("adaf");
		
		uBO.inserir(user);
		user.setNome("Thiago");
		uBO.excluir(uBO.obterPorId(user.getId()));
		Usuario userBanco = uBO.obterPorId(user.getId());
		Assert.assertTrue(userBanco == null);
	}

	@Test
	public void obterUsuarioPorNome() {
		UsuarioBO uBO = new UsuarioBO();
		Usuario user = new Usuario();
		user.setEmail("thiagolyma@gmail.com");
		user.setNome("Thiago Lima");
		user.setSenha("123abc");
		
		uBO.inserir(user);
		Usuario userBanco = uBO.obterPorNome(user.getNome());
		Assert.assertTrue(userBanco != null);
		Assert.assertTrue(userBanco.getId() != null);
		uBO.excluir(userBanco);
	}

	@Test
	public void alterarUsuario() {
		final String NOVO_NOME = "Orlando";
		UsuarioBO uBO = new UsuarioBO();
		Usuario user = new Usuario();
		user.setEmail("vavajunior@gmail.com");
		user.setNome("Osvaldo");
		user.setSenha("asdfad");
		uBO.inserir(user);
		Usuario userBanco = uBO.obterPorId(user.getId());
		userBanco.setNome(NOVO_NOME);
		uBO.atualizar(userBanco);
		Assert.assertTrue(userBanco.getNome() == NOVO_NOME);
		Assert.assertTrue(userBanco.getId() != null);
		Assert.assertTrue(userBanco.getId() == user.getId());
		
		//N�o est� excluindo
		uBO.excluir(userBanco);
	}
}
