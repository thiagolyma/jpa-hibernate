package br.ucb.financasonline.bo;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import br.ucb.financasonline.entidade.Banco;
import br.ucb.financasonline.negocio.BancoBO;

public class BancoBOTest {

	private Banco banco;
	private BancoBO bancoBo;

	@Before
	public void inicialize() {
		// Tem que ajustar a coluna ID, pois n�o est� auto incremental
		banco = new Banco();
		banco.setNome("Caixa Econ�mica Federal");
		banco.setNumero("104");
		banco.setAgencia("2458");

		bancoBo = new BancoBO();
	}

	@After
	public void tearDown() {

	}

	@Test
	public void inserirBanco() {
		bancoBo.inserir(banco);

		Assert.assertTrue(banco.getId() != null);
	}

	@Test
	public void obterBanco() {
		bancoBo.inserir(banco);

		Banco bancoTest = bancoBo.obterPorId(banco.getId());
		Assert.assertTrue(bancoTest != null);
		Assert.assertTrue(bancoTest.getId() == banco.getId());
	}

	@Test
	public void alterarBanco() {
		bancoBo.inserir(banco);

		banco.setNome("Banco do Brasil");
		bancoBo.atualizar(banco);
		Assert.assertEquals("Banco do Brasil", banco.getNome());
	}

	@Test
	public void excluirBanco() {
		bancoBo.inserir(banco);
		Long id = banco.getId();
		bancoBo.excluir(banco);
		Banco bancoTest = bancoBo.obterPorId(id);
		Assert.assertTrue(bancoTest == null);
	}
}
