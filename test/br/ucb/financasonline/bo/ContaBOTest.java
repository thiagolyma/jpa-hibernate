package br.ucb.financasonline.bo;

import java.util.Date;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import br.ucb.financasonline.entidade.Conta;
import br.ucb.financasonline.enumerador.Categoria;
import br.ucb.financasonline.enumerador.TipoPessoa;
import br.ucb.financasonline.negocio.ContaBO;

public class ContaBOTest {

	private Conta conta;
	private ContaBO contaBo;

	@Before
	public void inicialize() {
		conta = new Conta();
		conta.setCategoria(Categoria.ATIVA);
		conta.setData_cadastro(new Date(2013, 11, 03));
		conta.setDescricao("Conta 1");
		conta.setTipo(TipoPessoa.FISICA);
		
		contaBo = new ContaBO();
	}

	@After
	public void tearDown() {
		contaBo.excluir(conta);
	}

	@Test
	public void inserirConta() {

		contaBo.inserir(conta);		
		Assert.assertTrue(conta.getId() != null);		
	}

	@Test
	public void obterConta() {
		contaBo.inserir(conta);

		Conta contaTest = contaBo.obterPorId(conta.getId());
		Assert.assertTrue(contaTest != null);
		Assert.assertTrue(contaTest.getId() == conta.getId());
	}

	@Test
	public void alterarConta() {
		contaBo.inserir(conta);

		conta.setDescricao("Conta alterada");
		contaBo.atualizar(conta);
		Assert.assertEquals("Conta alterada", conta.getDescricao());
	}

	@Test
	public void excluirConta() {
		contaBo.inserir(conta);
		Long id = conta.getId();
		contaBo.excluir(conta);
		
		Conta contaTest = contaBo.obterPorId(id);
		Assert.assertTrue(contaTest == null);
	}
}
