import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GerarBancoDeDados {


	public static void main(String[] args) {
		Properties cfg = new Properties();
		cfg.setProperty("hibernate.hbm2ddl.auto", "create");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-exemplos",cfg);
		EntityManager em = emf.createEntityManager();
		
		//Pessoa pessoa = em.find(Pessoa.class,new Long(1));
		
		////System.out.println(pessoa.getId());
		////System.out.println(pessoa.getNome());
		
		
		em.close();
		emf.close();
		
	}

}
