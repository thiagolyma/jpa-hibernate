package br.ucb.tmor.exemplo.entidade.associacao.onetomany;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity(name="FILHOEXEMPLO")
public class FilhoExemplo {
	@Id	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_filho")
	private Long id;
	
	private String nome;
	
		
	@ManyToOne (fetch=FetchType.EAGER)
	@JoinColumn(name="id_pai",nullable=false)
	private PaiExemplo pai;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the pai
	 */
	public PaiExemplo getPai() {
		return pai;
	}

	/**
	 * @param pai the pai to set
	 */
	public void setPai(PaiExemplo pai) {
		this.pai = pai;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

}
