package br.ucb.tmor.exemplo.entidade.associacao.onetomany;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		PaiExemplo pai = new PaiExemplo();
		pai.setNome("Nome pai");
		
		FilhoExemplo filhoUm = new FilhoExemplo();
		filhoUm.setNome("Filho um");
		filhoUm.setPai(pai);
		
		FilhoExemplo filhoDois = new FilhoExemplo();
		filhoDois.setNome("Filho dois");
		filhoDois.setPai(pai);
		
		
		Set<FilhoExemplo> filhos = new HashSet<FilhoExemplo>();
		filhos.add(filhoUm);
		filhos.add(filhoDois);
		
	
		pai.setFilhos(filhos);
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-projeto-final-exemplos");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.persist(pai);
		
		em.getTransaction().commit();
		
		
		
		
		PaiExemplo find = em.find(PaiExemplo.class, new Long(1));
		System.out.println(find.getNome());
		Set<FilhoExemplo> filhosss = find.getFilhos();
		System.out.println(filhosss);

	}

}
