package br.ucb.tmor.exemplo.entidade.associacao.onetomany;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PAIEXEMPLO")
public class PaiExemplo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pai")
	private Long id;

	private String nome;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "pai")
	private Set<FilhoExemplo> filhos;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the filhos
	 */
	public Set<FilhoExemplo> getFilhos() {
		return filhos;
	}

	/**
	 * @param filhos
	 *            the filhos to set
	 */
	public void setFilhos(Set<FilhoExemplo> filhos) {
		this.filhos = filhos;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

}
