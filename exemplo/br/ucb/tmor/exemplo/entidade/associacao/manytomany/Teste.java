package br.ucb.tmor.exemplo.entidade.associacao.manytomany;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-projeto-final-exemplos");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		
		PessoaComEndereco pessoa = new PessoaComEndereco();
		pessoa.setNome("Nome pessoa");

		Endereco enderecoUm = new Endereco();
		enderecoUm.setNome("Nome endereco um");
		
		Endereco enderecoDois = new Endereco();
		enderecoDois.setNome("Nome endereco dois");
		
		Set<Endereco> enderecos = new HashSet<Endereco>();
		enderecos.add(enderecoUm);
		enderecos.add(enderecoDois);
		
		pessoa.setEndereco(enderecos);
		
		em.persist(pessoa);

		em.flush();
		em.getTransaction().commit();

		em.close();
		emf.close();
	}
}
