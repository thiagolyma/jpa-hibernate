package br.ucb.tmor.exemplo.entidade.associacao.manytomany;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
@Entity(name="PESSOA_COM_ENDERECO")
public class PessoaComEndereco{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String nome;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="pessoa_endereco",
	joinColumns=  @JoinColumn( name = "pessoa_id"),
	inverseJoinColumns= @JoinColumn(name = "endereco_id") )
	private Set<Endereco> endereco;

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the endereco
	 */
	public Set<Endereco> getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(Set<Endereco> endereco) {
		this.endereco = endereco;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
		
}
