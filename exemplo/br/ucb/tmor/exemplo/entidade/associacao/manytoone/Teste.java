package br.ucb.tmor.exemplo.entidade.associacao.manytoone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("jpa-hibernate-projeto-final-exemplos");
		EntityManager em = emf.createEntityManager();

		ClienteM cliente = new ClienteM();
		cliente.setNome("Jos�");

		ProdutoM produto = new ProdutoM();
		produto.setNome("Camisa de banda");

		ItemM item = new ItemM();
		item.setQuantidade(10);

		item.setCliente(cliente);
		item.setProduto(produto);

		em.getTransaction().begin();
		em.persist(item);
		em.getTransaction().commit();

		em.close();
	}
}
