package br.com.ucb.tmor.tabelaporsubclasse;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		PessoaFisicaTabelaPorSubClasse pessoaFisica = new PessoaFisicaTabelaPorSubClasse();
		pessoaFisica.setCpf("999.999.999-99");
		pessoaFisica.setDataNascimento(new Date());
		pessoaFisica.setNome("Nome Pessoa F�sica");
		
		PessoaJuridicaTabelaPorSubClasse pessoaJuridica = new PessoaJuridicaTabelaPorSubClasse();
		pessoaJuridica.setCnpj("99.999.999/9999-99");
		pessoaJuridica.setDataNascimento(new Date());
		pessoaJuridica.setNome("Nome Pessoa Juridica");
		pessoaJuridica.setNomeFantasia("Nome Fantasia Pessoa Juridica");
		

		em.persist(pessoaFisica);
		em.persist(pessoaJuridica);

		em.flush();
		em.getTransaction().commit();

		em.close();
		emf.close();
	}
}
