package br.com.ucb.tmor.tabelaporsubclasse;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id", referencedColumnName="id")
public class PessoaFisicaTabelaPorSubClasse extends PessoaAbstrataTabelaPorSubClasse {
	private String cpf;

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
