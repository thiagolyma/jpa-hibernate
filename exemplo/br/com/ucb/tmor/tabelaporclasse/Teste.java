package br.com.ucb.tmor.tabelaporclasse;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {
	public static void main(String[] args) {
		inserirPessoaFisica();
		
		inserirPessoaJuridica();
	}

	private static void inserirPessoaJuridica() {
		EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager sessao = sessionFactory.createEntityManager();
		sessao.getTransaction().begin();
		PessoaJuridicaTabelaPorClasse pessoaJuridica = new PessoaJuridicaTabelaPorClasse();
		pessoaJuridica.setCnpj("99.999.999/9999-99");
		pessoaJuridica.setDataNascimento(new Date());
		pessoaJuridica.setNome("Nome Pessoa Juridica");
		pessoaJuridica.setNomeFantasia("Nome Fantasia Pessoa Juridica");
		sessao.persist(pessoaJuridica);
		sessao.getTransaction().commit();
		sessao.close();
		sessionFactory.close();
	}

	private static void inserirPessoaFisica() {
		EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager sessao = sessionFactory.createEntityManager();
		sessao.getTransaction().begin();
		PessoaFisicaTabelaPorClasse pessoaFisica = new PessoaFisicaTabelaPorClasse();
		pessoaFisica.setCpf("999.999.999-99");
		pessoaFisica.setDataNascimento(new Date());
		pessoaFisica.setNome("Nome Pessoa F�sica");
		sessao.persist(pessoaFisica);
		sessao.getTransaction().commit();
		sessao.close();
		sessionFactory.close();
	}
}
