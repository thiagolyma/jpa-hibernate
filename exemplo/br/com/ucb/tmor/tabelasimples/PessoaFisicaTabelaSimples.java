package br.com.ucb.tmor.tabelasimples;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="PESSOA_FISICA")
public class PessoaFisicaTabelaSimples extends PessoaAbstrataTabelaSimples {
	private String cpf;

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
