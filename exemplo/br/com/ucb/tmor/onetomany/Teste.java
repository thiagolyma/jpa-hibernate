package br.com.ucb.tmor.onetomany;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager sessao = sessionFactory.createEntityManager();
		
		Produto produto = new Produto();
		produto.setNome("Nome produto um");
		Item itemUm = new Item();
		itemUm.setNome("Nome item um");
		Item itemDois = new Item();
		itemDois.setNome("Nome item dois");
		
		Set<Item> itens = new HashSet<Item>();
		itens.add(itemUm);
		itens.add(itemDois);
		produto.setItens(itens);
		sessao.getTransaction().begin();
		sessao.persist(produto);
		sessao.getTransaction().commit();
			
		Produto find = sessao.find(Produto.class, new Long(1));
		
		System.out.println(find.getId());
		
		sessao.close();
		sessionFactory.close();
	}
}
