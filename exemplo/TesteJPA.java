import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class TesteJPA {

	public static void main(String[] args) {
		//GerarBancoDeDados.main(args);
		
		inserirPessoa();
		
		pesquisarPessoaPorID();
		
		pesquisarTodasAsPessoas();
		
		pesquisarPessoaPorParametro();
		
		deletePessoa();
		
		inserirPessoa();
		
		deletePessoaPorParametro();		
	}

	public static void inserirPessoa() {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

		Pessoa pessoa = new Pessoa();
		pessoa.setId(new Long(1));
		pessoa.setNome("Nome 1");
		pessoa.setTelefone("Telefone 1");
		em.persist(pessoa);

		// em.flush();
		em.getTransaction().commit();
		em.close();
		emf.close();
	}

	public static void pesquisarPessoaPorID() {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager em = emf.createEntityManager();
		Pessoa pessoa = em.find(Pessoa.class, new Long(1));

		System.out.println(pessoa.getId());
		System.out.println(pessoa.getNome());
		System.out.println(pessoa.getTelefone());

		em.close();
		emf.close();
	}

	public static void pesquisarTodasAsPessoas() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager sessao = factory.createEntityManager();
		Query createQuery = sessao.createQuery("from Pessoa");
		
		Pessoa pessoaConsultada = (Pessoa) createQuery.getSingleResult();

		System.out.println(pessoaConsultada.getId());
		System.out.println(pessoaConsultada.getNome());
		System.out.println(pessoaConsultada.getTelefone());

		sessao.close();
		factory.close();
	}

	public static void pesquisarPessoaPorParametro() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager sessao = factory.createEntityManager();
		
		Query createQuery = sessao.createQuery("from Pessoa pessoa where pessoa.id=:variavel");
		createQuery.setParameter("variavel", new Long(1));

		Pessoa pessoaConsultada = (Pessoa) createQuery.getSingleResult();

		System.out.println(pessoaConsultada.getId());
		System.out.println(pessoaConsultada.getNome());
		System.out.println(pessoaConsultada.getTelefone());

		sessao.close();
		factory.close();
	}
	
	public static void deletePessoa() {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Pessoa pessoaConsultada = em.find(Pessoa.class, new Long(1));

		em.remove(pessoaConsultada);

		em.getTransaction().commit();
		
		em.close();
		emf.close();
	}
	
	public static void deletePessoaPorParametro() {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("jpa-hibernate-exemplos");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Query createQuery = em.createQuery("from Pessoa pessoa where pessoa.id=:id");
		createQuery.setParameter("id", new Long(1));

		Pessoa pessoaConsultada = (Pessoa) createQuery.getSingleResult();
		
		em.remove(pessoaConsultada);

		em.getTransaction().commit();
		
		em.close();
		emf.close();
	}
}
