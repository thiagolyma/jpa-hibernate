import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class PessoaJuridica extends PessoaAbstrata {

	@Column
	private String cnpj;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
