package br.ucb.financasonline.persistencia.dao;

import java.util.Collection;

import br.ucb.financasonline.entidade.Entidade;

public interface IDAOAbstrato<E extends Entidade> {
	public void inserir(E entidade) ;
	public void atualizar(E entidade);
	public void excluir(E entidade);
	public E obterPorId(Long id);
	public E obterPorNome(String entidade);
	public Collection<E> consultarTodos();
	public Collection<E> consultarTodosOrdenadoPorNome();
	public Collection<E> consultarTodosPorParteDoNome(String nome);
	public Collection<E> consultarTodosPorParteDoNomeOrdenado(String nome);
	public void removeTodos();
}
