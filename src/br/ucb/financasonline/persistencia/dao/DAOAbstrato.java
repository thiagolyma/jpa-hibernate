package br.ucb.financasonline.persistencia.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.ucb.financasonline.entidade.Entidade;

public abstract class DAOAbstrato<E extends Entidade> implements IDAOAbstrato<E>{
	private static EntityManagerFactory sessionFactory;
	private EntityManager session;
	private Class<E> classePersistente;
	private final Type superClasseGenerica;
    private final ParameterizedType tipoParametrizado;
    
    /**
     * Contrutor que guarda o tipo atual da Classe T.
     */
    @SuppressWarnings("unchecked")
	public DAOAbstrato() {
            superClasseGenerica = getClass().getGenericSuperclass();
            tipoParametrizado = ((ParameterizedType) superClasseGenerica);
            setClassePersistente((Class<E>) tipoParametrizado.getActualTypeArguments()[0]);
    }
    private void setClassePersistente(Class<E> classePersistente) {
		this.classePersistente = classePersistente;
	}
	private Class<E> getClassePersistente() {
		return classePersistente;
	}
	@SuppressWarnings("unused")
	private Type getGenericSuperclass() {
		return superClasseGenerica;
	}
	@SuppressWarnings("unused")
	private ParameterizedType getParameterizedType() {
		return tipoParametrizado;
	}
	private static EntityManagerFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = Persistence.createEntityManagerFactory("financasonline");
		}
		return sessionFactory;
	}
	public void setSession(EntityManager session) {
		this.session = session;
	}
	public EntityManager getSession() {
		if (session == null) {
			session = criarSession();
		}
		return session;
	}
	public EntityManager criarSession() {
		return getSessionFactory().createEntityManager();
	}
	public void finalizarSession() {
		getSession().close();
		setSession(null);
	}
	private EntityTransaction getTransacao() {
		return getSession().getTransaction();
	}
	public void iniciarTransacao() {
		getTransacao().begin();
	}
	public void commit() {
		getTransacao().commit();
	}
	public void rollback() {
		getTransacao().rollback();
	}
	
	public static void inicializarSessionFactory(){
		getSessionFactory();
	}
	
	@Override
	public void inserir(E entidade) {
		getSession().persist(entidade);
	}

	@Override
	public void atualizar(E entidade) {
		getSession().merge(entidade);
	}

	@Override
	public void excluir(E entidade) {
		getSession().remove(entidade);

	}

	@Override
	public E obterPorId(Long id) {
		return getSession().find(getClassePersistente(), id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public E obterPorNome(String nome) {
		String sql = getHQLPadrao()  + " where nome = '" + nome  +"'";
		Query query = createQuery(sql);
		return (E) query.getSingleResult();
	}
	private String getHQLPadrao(){
		String nomeClassePersistenteComPacoteSemClass =  getClassePersistente().toString().replace("class"," ");
		String hql = "from " +  nomeClassePersistenteComPacoteSemClass;
		return hql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<E> consultarTodos() {
		Query query = createQuery();
		return query.getResultList();
	}
	
	private Query createQuery(){
		Query query = getSession().createQuery(getHQLPadrao());
		return query;
	}
	private Query createQuery(String sql){
		Query query = getSession().createQuery(sql);
		return query;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Collection<E> consultarTodosOrdenadoPorNome() {
		String hql = getHQLPadrao()  + " order by nome";
		Query query = createQuery(hql);
		return query.getResultList();
	}
	@Override
	public void removeTodos(){
		String hql = "delete from " + getClassePersistente().toString().replace("class"," ");
		Query query = createQuery(hql);
		query.executeUpdate();
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public Collection<E> consultarTodosPorParteDoNome(String nome) {
		String hql = getHQLPadrao()  + " where in = '" + nome +"'";
		Query query = createQuery(hql);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<E> consultarTodosPorParteDoNomeOrdenado(String nome) {
		String hql = getHQLPadrao()  + " where in = '" + nome + "' order by nome";
		Query query = createQuery(hql);
		return query.getResultList();
	}

}