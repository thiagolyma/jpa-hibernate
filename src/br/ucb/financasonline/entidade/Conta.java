package br.ucb.financasonline.entidade;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import br.ucb.financasonline.enumerador.Categoria;
import br.ucb.financasonline.enumerador.TipoPessoa;

@Entity
public class Conta extends Entidade {

	private String descricao;
	private double valor;
	private Date data_cadastro;
	private boolean paga;
	private int numero_parcela;
	
	private TipoPessoa tipo;
	private Categoria categoria;
	
	@Transient
	//@ManyToOne
	//@JoinColumn(columnDefinition="usuariofk")
	private Usuario usuario;
	@Transient
	//@ManyToOne
	//@JoinColumn(columnDefinition="pessoafk")
	private CredorDevedor Pessoa;
	@Transient
	//@ManyToOne
	//@JoinColumn(columnDefinition="caixafk")
	private Caixa caixa;

	public CredorDevedor getPessoa() {
		return Pessoa;
	}

	public void setPessoa(CredorDevedor pessoa) {
		Pessoa = pessoa;
	}

	public TipoPessoa getTipo() {
		return tipo;
	}

	public void setTipo(TipoPessoa tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public int getNumero_parcela() {
		return numero_parcela;
	}

	public void setNumero_parcela(int numero_parcela) {
		this.numero_parcela = numero_parcela;
	}

	public boolean isPaga() {
		return paga;
	}

	public void setPaga(boolean paga) {
		this.paga = paga;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
