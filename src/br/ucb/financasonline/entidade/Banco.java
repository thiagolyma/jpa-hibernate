package br.ucb.financasonline.entidade;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Banco extends Entidade {

	private String numero;
	private String agencia;
	private String nome;
	
	//@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@Transient
	private Set<Caixa> Caixas;

	public Set<Caixa> getCaixas() {
		return Caixas;
	}

	public void setCaixas(Set<Caixa> caixas) {
		Caixas = caixas;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
