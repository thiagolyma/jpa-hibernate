package br.ucb.financasonline.entidade;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Usuario extends Entidade {

	private String email;
	private String senha;
	private String nome;

	@Transient
	//@OneToMany(cascade = CascadeType.ALL, fetch= FetchType.LAZY)
	private Set<Conta> Contas;

	public String getEmail() {
		return email;
	}

	public Set<Conta> getContas() {
		return Contas;
	}

	public void setContas(Set<Conta> contas) {
		Contas = contas;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
