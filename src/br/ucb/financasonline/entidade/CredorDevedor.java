package br.ucb.financasonline.entidade;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import br.ucb.financasonline.enumerador.TipoPessoa;

@Entity
@Table(name = "credor_devedor")
public class CredorDevedor extends Entidade {

	@Column(name = "ativo", nullable = false)
	private boolean ativo;
	private double valorLimiteCredito;
	private TipoPessoa tipo;
	private String nome;
	private String logradouro;
	private String foneCelular;
	private String foneFixo;

	@OneToMany(cascade = CascadeType.ALL, fetch= FetchType.LAZY)
	private Set<Conta> Contas;
	
	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public double getValorLimiteCredito() {
		return valorLimiteCredito;
	}

	public void setValorLimiteCredito(double valorLimiteCredito) {
		this.valorLimiteCredito = valorLimiteCredito;
	}

	public TipoPessoa getTipo() {
		return tipo;
	}

	public void setTipo(TipoPessoa tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getFoneCelular() {
		return foneCelular;
	}

	public void setFoneCelular(String foneCelular) {
		this.foneCelular = foneCelular;
	}

	public String getFoneFixo() {
		return foneFixo;
	}

	public void setFoneFixo(String foneFixo) {
		this.foneFixo = foneFixo;
	}
}
