package br.ucb.financasonline.entidade;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Caixa extends Entidade {

	private String descricao;
	private double saldo;
	private Date data;
	
	//@ManyToOne
	@Transient
	private Banco banco;

	//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Transient
	private Set<Conta> Contas;
	
	public Set<Conta> getContas() {
		return Contas;
	}

	public void setContas(Set<Conta> contas) {
		Contas = contas;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}