package br.ucb.financasonline.negocio;

import br.ucb.financasonline.entidade.Quitacao;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;
import br.ucb.financasonline.persistencia.dao.QuitacaoDAO;

public class QuitacaoBO extends BOAbstrato<Quitacao> {

	@Override
	protected DAOAbstrato<Quitacao> doGetDaoAbstrato() {
		return new QuitacaoDAO();
	}

	@Override
	public void validaDados(Quitacao quitacao) throws Exception {

	}
}
