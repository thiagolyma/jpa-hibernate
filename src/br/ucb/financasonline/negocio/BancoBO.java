package br.ucb.financasonline.negocio;

import br.ucb.financasonline.entidade.Banco;
import br.ucb.financasonline.persistencia.dao.BancoDAO;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;

public class BancoBO extends BOAbstrato<Banco> {

	@Override
	protected DAOAbstrato<Banco> doGetDaoAbstrato() {
		return new BancoDAO();
	}

	@Override
	public void validaDados(Banco banco) throws Exception {

	}
}
