package br.ucb.financasonline.negocio;

import br.ucb.financasonline.entidade.Conta;
import br.ucb.financasonline.persistencia.dao.ContaDAO;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;

public class ContaBO extends BOAbstrato<Conta> {

	@Override
	protected DAOAbstrato<Conta> doGetDaoAbstrato() {
		return new ContaDAO();
	}

	@Override
	public void validaDados(Conta entidade) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
