package br.ucb.financasonline.negocio;

import br.ucb.financasonline.entidade.Caixa;
import br.ucb.financasonline.persistencia.dao.CaixaDAO;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;

public class CaixaBO extends BOAbstrato<Caixa>{

	@Override
	protected DAOAbstrato<Caixa> doGetDaoAbstrato() {
		return new CaixaDAO();
	}

	@Override
	public void validaDados(Caixa entidade) throws Exception {
		// TODO Auto-generated method stub
	}
}
