package br.ucb.financasonline.negocio;

import br.ucb.financasonline.comum.Mensagem;
import br.ucb.financasonline.entidade.Usuario;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;
import br.ucb.financasonline.persistencia.dao.UsuarioDAO;

public class UsuarioBO extends BOAbstrato<Usuario> {

	@Override
	protected DAOAbstrato<Usuario> doGetDaoAbstrato() {
		return new UsuarioDAO();
	}

	@Override
	public void validaDados(Usuario usuario) throws Exception{
		if (usuario.getEmail().toString().length() ==0 ||
				usuario.getNome().toString().length() == 0 ||
				usuario.getSenha().toString().length() == 0){
			throw new Exception(Mensagem.DADOS_USUARIO_INVALIDO);
		}
	}	
}
