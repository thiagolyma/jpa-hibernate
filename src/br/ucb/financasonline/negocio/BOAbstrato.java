package br.ucb.financasonline.negocio;

import java.util.Collection;

import javax.persistence.EntityManager;

import br.ucb.financasonline.entidade.Entidade;
import br.ucb.financasonline.persistencia.dao.DAOAbstrato;

public abstract class BOAbstrato<E extends Entidade> implements IBOAbstrato<E> {

	private DAOAbstrato<E> daoAbstrato;

	protected DAOAbstrato<E> getDaoAbstrato() {
		if (daoAbstrato == null) {
			daoAbstrato = doGetDaoAbstrato();
		}
		return daoAbstrato;
	}

	protected abstract DAOAbstrato<E> doGetDaoAbstrato();

	protected void iniciarTransacao() {
		getDaoAbstrato().iniciarTransacao();
	}

	public EntityManager criarSession() {
		return getDaoAbstrato().criarSession();
	}

	protected void finalizarSession() {
		getDaoAbstrato().finalizarSession();
	}

	protected void commit() {
		getDaoAbstrato().commit();
	}

	protected void rollback() {
		getDaoAbstrato().rollback();
	}

	public void inserir(E entidade) {
		try {
			validaDados(entidade);
			iniciarTransacao();
			getDaoAbstrato().inserir(entidade);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
		} finally {
			finalizarSession();
		}
	}

	public void atualizar(E entidade) {
		try {
			validaDados(entidade);
			iniciarTransacao();
			getDaoAbstrato().atualizar(entidade);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
		} finally {
			finalizarSession();
		}
	}

	public void excluir(E entidade) {
		try {
			iniciarTransacao();
			getDaoAbstrato().excluir(entidade);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
		} finally {
			finalizarSession();
		}
	}

	public E obterPorId(Long id) {
		return (E) getDaoAbstrato().obterPorId(id);
	}

	public E obterPorNome(String nome) {
		return (E) getDaoAbstrato().obterPorNome(nome);
	}

	public Collection<E> consultarTodos() {
		return (Collection<E>) getDaoAbstrato().consultarTodos();
	}

	public Collection<E> consultarTodosOrdenadoPorNome() {
		return (Collection<E>) getDaoAbstrato().consultarTodosOrdenadoPorNome();
	}

	public Collection<E> consultarTodosPorParteDoNome(String nome) {
		return (Collection<E>) getDaoAbstrato().consultarTodosPorParteDoNome(
				nome);
	}

	public Collection<E> consultarTodosPorParteDoNomeOrdenado(String nome) {
		return (Collection<E>) getDaoAbstrato()
				.consultarTodosPorParteDoNomeOrdenado(nome);
	}

	protected void setSession(EntityManager session) {
		getDaoAbstrato().setSession(session);
	}

	public abstract void validaDados(E entidade) throws Exception;
}
