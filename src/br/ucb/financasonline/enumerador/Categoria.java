package br.ucb.financasonline.enumerador;

public enum Categoria {
	ATIVA,
    BLOQUEADA,
    INATIVA,
    ENCERRADA;
	
    public Categoria[] getValores(){
            return Categoria.values();
    }
}
