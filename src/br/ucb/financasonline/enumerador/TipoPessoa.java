package br.ucb.financasonline.enumerador;

public enum TipoPessoa {
	FISICA(1, "Física"),
	JURIDICA(2, "Jurídica"),
	USUARIO(3,"Usuário");

	private Integer chave;
	private String valor;

	private TipoPessoa(Integer chave, String valor) {
		this.chave = chave;
		this.valor = valor;
	}

	public Integer getChave() {
		return chave;
	}

	public String getValor() {
		return valor;
	}
}
