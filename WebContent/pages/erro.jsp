<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ocorreu um erro!</title>
</head>
<body>
	<h2 align="center">Houve um problema ao tentar executar sua solicita&ccedil;&atilde;o.</h2>
	<br/>
	<h4>Relate a mensagem abaixo ao desenvolvimento:</h4>
	<font color="red" size="5px">"<%=exception.getMessage()%>"</font>
	<br/>
	<div style="padding-top: 30%">
		<a href="mailto:thiagolyma@gmail.com?subject=Problemas%20no%20DashBoard&body=<%=exception.getMessage()%>">Thiago Lima - thiagolyma@gmail.com,</a><br/>
	</div>
</body>
</html>